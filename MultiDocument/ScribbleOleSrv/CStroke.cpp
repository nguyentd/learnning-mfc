#include "stdafx.h"
#include "CStroke.h"


IMPLEMENT_SERIAL(CStroke,CObject,2)
CStroke::CStroke()
{
}

CStroke::CStroke(UINT nPenWidth)
{
	m_nPenWidth = nPenWidth;
	m_rectBounding.SetRectEmpty();
}

CStroke::~CStroke()
{
}


void CStroke::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{	// storing code
		ar << m_rectBounding;
		ar << (WORD)m_nPenWidth;
		m_poinArray.Serialize(ar);
	}
	else
	{	// loading code
		ar >> m_rectBounding;
		WORD w;
		ar >> w;
		m_nPenWidth = w;
		m_poinArray.Serialize(ar);
	}
}

BOOL CStroke::DrawStroke(CDC* pDC)
{
	CPen penStroke;
	if (!penStroke.CreatePen(PS_SOLID, m_nPenWidth, RGB(0, 0, 0)))
		return FALSE;
	CPen* pOldPen = pDC->SelectObject(&penStroke);
	pDC->MoveTo(m_poinArray[0]);
	for (int i = 0; i < m_poinArray.GetSize(); i++)
	{
		pDC->LineTo(m_poinArray[i]);
	}
	pDC->SelectObject(pOldPen);
	return TRUE;
}

CRect& CStroke::GetBoundingRect()
{
	// TODO: Add your implementation code here.
	// TODO: insert return statement here
	return m_rectBounding;
}


void CStroke::FinishStroke()
{
	// TODO: Add your implementation code here.
	if (m_poinArray.GetSize() == 0)
	{
		m_rectBounding.SetRectEmpty();
		return;
	}

	CPoint pt = m_poinArray[0];
	m_rectBounding = CRect(pt.x, pt.y, pt.x, pt.y);
	for (int i = 1; i < m_poinArray.GetSize(); i++)
	{
		pt = m_poinArray[i];
		m_rectBounding.left = min(m_rectBounding.left, pt.x);
		m_rectBounding.right = max(m_rectBounding.right, pt.x);
		m_rectBounding.top = min(m_rectBounding.top, pt.y);
		m_rectBounding.bottom = max(m_rectBounding.bottom, pt.y);
		m_rectBounding.InflateRect(CSize(m_nPenWidth, m_nPenWidth));

	}
}
