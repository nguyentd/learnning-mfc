
// ScribbleOleSrvView.cpp : implementation of the CScribbleOleSrvView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "ScribbleOleSrv.h"
#endif

#include "ScribbleOleSrvDoc.h"
#include "CntrItem.h"
#include "resource.h"
#include "ScribbleOleSrvView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CScribbleOleSrvView

IMPLEMENT_DYNCREATE(CScribbleOleSrvView, CScrollView)

BEGIN_MESSAGE_MAP(CScribbleOleSrvView, CScrollView)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_COMMAND(ID_OLE_INSERT_NEW, &CScribbleOleSrvView::OnInsertObject)
	ON_COMMAND(ID_CANCEL_EDIT_CNTR, &CScribbleOleSrvView::OnCancelEditCntr)
	ON_COMMAND(ID_FILE_PRINT, &CScribbleOleSrvView::OnFilePrint)
	ON_COMMAND(ID_CANCEL_EDIT_SRVR, &CScribbleOleSrvView::OnCancelEditSrvr)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CScrollView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

// CScribbleOleSrvView construction/destruction

CScribbleOleSrvView::CScribbleOleSrvView() noexcept
{
	m_pSelection = nullptr;
	// TODO: add construction code here

}

CScribbleOleSrvView::~CScribbleOleSrvView()
{
}

BOOL CScribbleOleSrvView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CScribbleOleSrvView drawing

void CScribbleOleSrvView::OnDraw(CDC* pDC)
{
	if (!pDC)
		return;

	CScribbleOleSrvDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	CRect rectClip;
	CRect rectStroke;
	pDC->GetClipBox(&rectClip);


	CTypedPtrList<CObList, CStroke*>&strokeList = pDoc->m_strokeList;
	POSITION pos = strokeList.GetHeadPosition();
	while (pos != NULL)
	{
		CStroke* pStroke = strokeList.GetNext(pos);
		rectStroke = pStroke->GetBoundingRect();
		if (!rectStroke.IntersectRect(&rectStroke, &rectClip))
			continue;
		pStroke->DrawStroke(pDC);
	}
	// TODO: add draw code for native data here
	// TODO: also draw all OLE items in the document

	// Draw the selection at an arbitrary position.  This code should be
	//  removed once your real drawing code is implemented.  This position
	//  corresponds exactly to the rectangle returned by CScribbleOleSrvCntrItem,
	//  to give the effect of in-place editing.

	// TODO: remove this code when final draw code is complete.
	/*if (m_pSelection != nullptr)
	{
		CSize size;
		CRect rect(10, 10, 210, 210);

		if (m_pSelection->GetExtent(&size, m_pSelection->m_nDrawAspect))
		{
			pDC->HIMETRICtoLP(&size);
			rect.right = size.cx + 10;
			rect.bottom = size.cy + 10;
		}
		m_pSelection->Draw(pDC, rect);
	}*/
}

void CScribbleOleSrvView::OnInitialUpdate()
{
	ResyncScrollSize();
	//SetScrollSizes(MM_TEXT, GetDocument()->GetDocSize());
	CScrollView::OnInitialUpdate();


	// TODO: remove this code when final selection model code is written
	m_pSelection = nullptr;    // initialize selection

}


// CScribbleOleSrvView printing

BOOL CScribbleOleSrvView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CScribbleOleSrvView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CScribbleOleSrvView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CScribbleOleSrvView::OnDestroy()
{
	// Deactivate the item on destruction; this is important
	// when a splitter view is being used
   COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
   if (pActiveItem != nullptr && pActiveItem->GetActiveView() == this)
   {
      pActiveItem->Deactivate();
      ASSERT(GetDocument()->GetInPlaceActiveItem(this) == nullptr);
   }
   CView::OnDestroy();
}



// OLE Client support and commands

BOOL CScribbleOleSrvView::IsSelected(const CObject* pDocItem) const
{
	// The implementation below is adequate if your selection consists of
	//  only CScribbleOleSrvCntrItem objects.  To handle different selection
	//  mechanisms, the implementation here should be replaced

	// TODO: implement this function that tests for a selected OLE client item

	return pDocItem == m_pSelection;
}

void CScribbleOleSrvView::OnInsertObject()
{
	// Invoke the standard Insert Object dialog box to obtain information
	//  for new CScribbleOleSrvCntrItem object
	COleInsertDialog dlg;
	if (dlg.DoModal() != IDOK)
		return;

	BeginWaitCursor();

	CScribbleOleSrvCntrItem* pItem = nullptr;
	TRY
	{
		// Create new item connected to this document
		CScribbleOleSrvDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);
		pItem = new CScribbleOleSrvCntrItem(pDoc);
		ASSERT_VALID(pItem);

		// Initialize the item from the dialog data
		if (!dlg.CreateItem(pItem))
			AfxThrowMemoryException();  // any exception will do
		ASSERT_VALID(pItem);

        if (dlg.GetSelectionType() == COleInsertDialog::createNewItem)
			pItem->DoVerb(OLEIVERB_SHOW, this);

		ASSERT_VALID(pItem);
		// As an arbitrary user interface design, this sets the selection
		//  to the last item inserted

		// TODO: reimplement selection as appropriate for your application
		m_pSelection = pItem;   // set selection to last inserted item
		pDoc->UpdateAllViews(nullptr);
	}
	CATCH(CException, e)
	{
		if (pItem != nullptr)
		{
			ASSERT_VALID(pItem);
			pItem->Delete();
		}
		AfxMessageBox(IDP_FAILED_TO_CREATE);
	}
	END_CATCH

	EndWaitCursor();
}

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the container (not the server) causes the deactivation
void CScribbleOleSrvView::OnCancelEditCntr()
{
	// Close any in-place active item on this view.
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != nullptr)
	{
		pActiveItem->Close();
	}
	ASSERT(GetDocument()->GetInPlaceActiveItem(this) == nullptr);
}

// Special handling of OnSetFocus and OnSize are required for a container
//  when an object is being edited in-place
void CScribbleOleSrvView::OnSetFocus(CWnd* pOldWnd)
{
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != nullptr &&
		pActiveItem->GetItemState() == COleClientItem::activeUIState)
	{
		// need to set focus to this item if it is in the same view
		CWnd* pWnd = pActiveItem->GetInPlaceWindow();
		if (pWnd != nullptr)
		{
			pWnd->SetFocus();   // don't call the base class
			return;
		}
	}

	CView::OnSetFocus(pOldWnd);
}

void CScribbleOleSrvView::OnSize(UINT nType, int cx, int cy)
{
	/*CView::OnSize(nType, cx, cy);
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != nullptr)
		pActiveItem->SetItemRects();*/
	SetScrollSizes(MM_TEXT, CSize(0, 0));
}

void CScribbleOleSrvView::OnFilePrint()
{
	//By default, we ask the Active document to print itself
	//using IOleCommandTarget. If you don't want this behavior
	//remove the call to COleDocObjectItem::DoDefaultPrinting.
	//If the call fails for some reason, we will try printing
	//the docobject using the IPrint interface.
	CPrintInfo printInfo;
	ASSERT(printInfo.m_pPD != nullptr);
	if (S_OK == COleDocObjectItem::DoDefaultPrinting(this, &printInfo))
		return;

	CView::OnFilePrint();

}


// OLE Server support

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the server (not the container) causes the deactivation
void CScribbleOleSrvView::OnCancelEditSrvr()
{
	GetDocument()->OnDeactivateUI(FALSE);
}


// CScribbleOleSrvView diagnostics

#ifdef _DEBUG
void CScribbleOleSrvView::AssertValid() const
{
	CView::AssertValid();
}

void CScribbleOleSrvView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CScribbleOleSrvDoc* CScribbleOleSrvView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CScribbleOleSrvDoc)));
	return (CScribbleOleSrvDoc*)m_pDocument;
}
#endif //_DEBUG


// CScribbleOleSrvView message handlers


void CScribbleOleSrvView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_pStrokeCur = GetDocument()->NewStroke();
	m_pStrokeCur->m_poinArray.Add(point);
	SetCapture();
	m_ptPrev = point;
	//CView::OnLButtonDown(nFlags, point);
}


void CScribbleOleSrvView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (GetCapture() != this)
		return;
	CScribbleOleSrvDoc * pDoc = GetDocument();
	CClientDC dc(this);
	CPen* pOldPen = dc.SelectObject(pDoc->GetCurrentPen());
	dc.MoveTo(m_ptPrev);
	dc.LineTo(point);
	dc.SelectObject(pOldPen);
	m_pStrokeCur->m_poinArray.Add(point);
	m_pStrokeCur->FinishStroke();
	pDoc->UpdateAllViews(this, 0L, m_pStrokeCur);
	ReleaseCapture();
	pDoc->NotifyChanged();
	//CView::OnLButtonUp(nFlags, point);
}


void CScribbleOleSrvView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (GetCapture() != this)
		return;
	CClientDC dc(this);
	m_pStrokeCur->m_poinArray.Add(point);
	CPen* pOldPen = dc.SelectObject(GetDocument()->GetCurrentPen());
	dc.MoveTo(m_ptPrev);
	dc.LineTo(point);
	dc.SelectObject(pOldPen);
	m_ptPrev = point;
	//CView::OnMouseMove(nFlags, point);
}


void CScribbleOleSrvView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pHint != NULL)
	{
		if (pHint->IsKindOf(RUNTIME_CLASS(CStroke)))
		{
			CStroke* pStroke = (CStroke*)pHint;
			CRect rectInvalid = pStroke->GetBoundingRect();
			InvalidateRect(&rectInvalid);
			return;
		}
	}
	Invalidate();
}


void CScribbleOleSrvView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: Add your specialized code here and/or call the base class
	CScribbleOleSrvDoc* pDoc = GetDocument();

	CScrollView::OnPrepareDC(pDC, pInfo);

	pDC->SetMapMode(MM_ANISOTROPIC);
	CSize sizeDoc = pDoc->GetDocSize();
	sizeDoc.cy = -sizeDoc.cy;
	pDC->SetWindowExt(sizeDoc);
	CSize sizeNum, sizeDenom;
	pDoc->GetZoomFactor(&sizeNum, &sizeDenom);
	int xLogPixPerInch = pDC->GetDeviceCaps(LOGPIXELSX);
	int yLogPixPerInch = pDC->GetDeviceCaps(LOGPIXELSY);
	long xExt = (long)sizeDoc.cx * xLogPixPerInch * sizeNum.cx;
	xExt /= 100 * (long)sizeDenom.cx;

	long yExt = (long)sizeDoc.cy * yLogPixPerInch * sizeNum.cy;
	yExt /= 100 * (long)sizeDenom.cy;
	pDC->SetViewportExt((int)xExt, (int)-yExt);
}


void CScribbleOleSrvView::ResyncScrollSize()
{
	// TODO: Add your implementation code here.
	CClientDC dc(NULL);
	OnPrepareDC(&dc);
	CSize sizeDoc = GetDocument()->GetDocSize();
	//dc.LPtoDP(&sizeDoc);
	SetScrollSizes(MM_TEXT, sizeDoc);
}
