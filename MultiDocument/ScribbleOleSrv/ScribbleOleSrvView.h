
// ScribbleOleSrvView.h : interface of the CScribbleOleSrvView class
//

#pragma once

class CScribbleOleSrvCntrItem;

class CScribbleOleSrvView : public CScrollView
{
protected: // create from serialization only
	CScribbleOleSrvView() noexcept;
	DECLARE_DYNCREATE(CScribbleOleSrvView)

// Attributes
public:
	CScribbleOleSrvDoc* GetDocument() const;
	// m_pSelection holds the selection to the current CScribbleOleSrvCntrItem.
	// For many applications, such a member variable isn't adequate to
	//  represent a selection, such as a multiple selection or a selection
	//  of objects that are not CScribbleOleSrvCntrItem objects.  This selection
	//  mechanism is provided just to help you get started

	// TODO: replace this selection mechanism with one appropriate to your app
	CScribbleOleSrvCntrItem* m_pSelection;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL IsSelected(const CObject* pDocItem) const;// Container support

// Implementation
public:
	virtual ~CScribbleOleSrvView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnInsertObject();
	afx_msg void OnCancelEditCntr();
	afx_msg void OnFilePrint();
	afx_msg void OnCancelEditSrvr();
	DECLARE_MESSAGE_MAP()
	CStroke* m_pStrokeCur;
	CPoint   m_ptPrev;
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	void ResyncScrollSize();
};

#ifndef _DEBUG  // debug version in ScribbleOleSrvView.cpp
inline CScribbleOleSrvDoc* CScribbleOleSrvView::GetDocument() const
   { return reinterpret_cast<CScribbleOleSrvDoc*>(m_pDocument); }
#endif

