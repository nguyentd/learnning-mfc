
// ScribbleDoc.h : interface of the CScribbleDoc class
//


#pragma once

#include "CStroke.h"
#include "CPenWidthsDlg.h"

class CScribbleDoc : public CDocument
{
protected: // create from serialization only
	CScribbleDoc() noexcept;
	DECLARE_DYNCREATE(CScribbleDoc)

// Attributes
public:
	CTypedPtrList<CObList, CStroke*> m_strokeList;
	CPen* GetCurrentPen() { return &m_penCur; }
// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CScribbleDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
	// Do rong net ve
	UINT m_nPenWidth;
	// Vi tri net but ve
	CPen m_penCur;
public:
	CStroke* NewStroke();
protected:
	void InitDocument();
public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual void DeleteContents();
	afx_msg void OnEditClearAll();
	afx_msg void OnPenThickOrThin();
protected:
	BOOL m_bThickPen;
	UINT m_nThickWidth;
	UINT m_nThinWidth;
	void ReplacePen();
	CSize m_sizeDoc;
public:
	afx_msg void OnUpdateEditClearAll(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePenThickOrThin(CCmdUI *pCmdUI);
	afx_msg void OnPenWidths();
	CSize GetDocSize() { return m_sizeDoc; }
};
