
// ScribbleOleSrvDoc.cpp : implementation of the CScribbleOleSrvDoc class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "ScribbleOleSrv.h"
#endif

#include "ScribbleOleSrvDoc.h"
#include "CntrItem.h"
#include "SrvrItem.h"
#include "CStroke.h"
#include "PenWidthsDlg.h"
#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CScribbleOleSrvDoc

IMPLEMENT_DYNCREATE(CScribbleOleSrvDoc, COleServerDoc)

BEGIN_MESSAGE_MAP(CScribbleOleSrvDoc, COleServerDoc)
	// Enable default OLE container implementation
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, &COleServerDoc::OnUpdatePasteMenu)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE_LINK, &COleServerDoc::OnUpdatePasteLinkMenu)
	ON_UPDATE_COMMAND_UI(ID_OLE_EDIT_CONVERT, &COleServerDoc::OnUpdateObjectVerbMenu)
	ON_COMMAND(ID_OLE_EDIT_CONVERT, &COleServerDoc::OnEditConvert)
	ON_UPDATE_COMMAND_UI(ID_OLE_EDIT_LINKS, &COleServerDoc::OnUpdateEditLinksMenu)
	ON_UPDATE_COMMAND_UI(ID_OLE_VERB_POPUP, &CScribbleOleSrvDoc::OnUpdateObjectVerbPopup)
	ON_COMMAND(ID_OLE_EDIT_LINKS, &COleServerDoc::OnEditLinks)
	ON_UPDATE_COMMAND_UI_RANGE(ID_OLE_VERB_FIRST, ID_OLE_VERB_LAST, &COleServerDoc::OnUpdateObjectVerbMenu)
	ON_COMMAND(ID_EDIT_COPY, &CScribbleOleSrvDoc::OnEditCopy)
	ON_COMMAND(ID_EDIT_CLEARALL, &CScribbleOleSrvDoc::OnEditClearall)
	ON_COMMAND(ID_PEN_THICK_OR_THINK, &CScribbleOleSrvDoc::OnPenThickOrThink)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CLEARALL, &CScribbleOleSrvDoc::OnUpdateEditClearall)
	ON_UPDATE_COMMAND_UI(ID_PEN_THICK_OR_THINK, &CScribbleOleSrvDoc::OnUpdatePenThickOrThink)
	ON_COMMAND(ID_PEN_PEN_WIDTHS, &CScribbleOleSrvDoc::OnPenPenWidths)
END_MESSAGE_MAP()


// CScribbleOleSrvDoc construction/destruction

CScribbleOleSrvDoc::CScribbleOleSrvDoc() noexcept
{
	// Use OLE compound files
	EnableCompoundFile();

	// TODO: add one-time construction code here

}

CScribbleOleSrvDoc::~CScribbleOleSrvDoc()
{
}

BOOL CScribbleOleSrvDoc::OnNewDocument()
{
	if (!COleServerDoc::OnNewDocument())
		return FALSE;
	InitDocument();
	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


// CScribbleOleSrvDoc server implementation

COleServerItem* CScribbleOleSrvDoc::OnGetEmbeddedItem()
{
	// OnGetEmbeddedItem is called by the framework to get the COleServerItem
	//  that is associated with the document.  It is only called when necessary.

	CScribbleOleSrvSrvrItem* pItem = new CScribbleOleSrvSrvrItem(this);
	ASSERT_VALID(pItem);
	return pItem;
}




// CScribbleOleSrvDoc serialization

void CScribbleOleSrvDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		ar << m_sizeDoc;
	}
	else
	{
		// TODO: add loading code here
		ar >> m_sizeDoc;
	}

	// Calling the base class COleServerDoc enables serialization
	//  of the container document's COleClientItem objects.
	//COleServerDoc::Serialize(ar);
	m_strokeList.Serialize(ar);
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CScribbleOleSrvDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CScribbleOleSrvDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data.
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CScribbleOleSrvDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = nullptr;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != nullptr)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CScribbleOleSrvDoc diagnostics

#ifdef _DEBUG
void CScribbleOleSrvDoc::AssertValid() const
{
	COleServerDoc::AssertValid();
}

void CScribbleOleSrvDoc::Dump(CDumpContext& dc) const
{
	COleServerDoc::Dump(dc);
}
#endif //_DEBUG


// CScribbleOleSrvDoc commands


CStroke* CScribbleOleSrvDoc::NewStroke()
{
	// TODO: Add your implementation code here.
	CStroke * pStrokeItem = new CStroke(m_nPenWidth);
	m_strokeList.AddTail(pStrokeItem);
	SetModifiedFlag();

	return pStrokeItem;
}


void CScribbleOleSrvDoc::InitDocument()
{
	// TODO: Add your implementation code here.
	//m_nPenWidth = 2;
	//m_penCur.CreatePen(PS_SOLID, m_nPenWidth, RGB(0, 0, 0));
	m_bThickPen = FALSE;
	m_nThinWidth = 2;
	m_nThickWidth = 5;
	ReplacePen();
	m_sizeDoc = CSize(200, 200);
}


BOOL CScribbleOleSrvDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if (!COleServerDoc::OnOpenDocument(lpszPathName))
		return FALSE;

	// TODO:  Add your specialized creation code here
	InitDocument();
	return TRUE;
}


void CScribbleOleSrvDoc::DeleteContents()
{
	// TODO: Add your specialized code here and/or call the base class
	while (!m_strokeList.IsEmpty())
	{
		delete m_strokeList.RemoveHead();
	}

	COleServerDoc::DeleteContents();
}


void CScribbleOleSrvDoc::OnEditCopy()
{
	// TODO: Add your command handler code here
	CScribbleOleSrvSrvrItem* pItem = GetEmbeddedItem();
	pItem->CopyToClipboard(TRUE);

}


void CScribbleOleSrvDoc::OnEditClearall()
{
	// TODO: Add your command handler code here
	DeleteContents();
	SetModifiedFlag();
	UpdateAllViews(NULL);
}


void CScribbleOleSrvDoc::OnPenThickOrThink()
{
	// TODO: Add your command handler code here
	m_bThickPen = !m_bThickPen;
	ReplacePen();
}


void CScribbleOleSrvDoc::ReplacePen()
{
	// TODO: Add your implementation code here.
	m_nPenWidth = m_bThickPen ? m_nThickWidth : m_nThinWidth;
	m_penCur.DeleteObject();
	m_penCur.CreatePen(PS_SOLID, m_nPenWidth, RGB(0, 0, 0));
}


void CScribbleOleSrvDoc::OnUpdateEditClearall(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(!m_strokeList.IsEmpty());
}


void CScribbleOleSrvDoc::OnUpdatePenThickOrThink(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->SetCheck(m_bThickPen);
}


void CScribbleOleSrvDoc::OnPenPenWidths()
{
	// TODO: Add your command handler code here
	CPenWidthsDlg dlg;
	dlg.m_nThinWidths = m_nThinWidth;
	dlg.m_nThickWidths = m_nThickWidth;
	if (dlg.DoModal() == IDOK)
	{
		m_nThinWidth = dlg.m_nThinWidths;
		m_nThickWidth = dlg.m_nThickWidths;
		ReplacePen();
	}
}


CSize CScribbleOleSrvDoc::GetDocSize()
{
	// TODO: Add your implementation code here.
	return m_sizeDoc;
}
