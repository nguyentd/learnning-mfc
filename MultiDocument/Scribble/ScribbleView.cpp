
// ScribbleView.cpp : implementation of the CScribbleView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "Scribble.h"
#endif

#include "ScribbleDoc.h"
#include "ScribbleView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CScribbleView

IMPLEMENT_DYNCREATE(CScribbleView, CScrollView)

BEGIN_MESSAGE_MAP(CScribbleView, CScrollView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

// CScribbleView construction/destruction

CScribbleView::CScribbleView() noexcept
{
	// TODO: add construction code here

}

CScribbleView::~CScribbleView()
{
}

BOOL CScribbleView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CScribbleView drawing

void CScribbleView::OnDraw(CDC* pDC)
{
	CScribbleDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	CRect rectClip;
	CRect rectStroke;
	pDC->GetClipBox(&rectClip);
	rectClip.InflateRect(1, 1);
	CTypedPtrList<CObList, CStroke*> &strokeList = pDoc->m_strokeList;
	POSITION pos = strokeList.GetHeadPosition();
	while (pos != NULL)
	{
		CStroke* pStroke = strokeList.GetNext(pos);
		rectStroke = pStroke->GetBoundingRect();
		pDC->LPtoDP(&rectStroke);
		rectStroke.InflateRect(1, 1);
		if (!rectStroke.IntersectRect(&rectStroke, &rectClip))
			continue;
		pStroke->DrawStroke(pDC);
	}
	// TODO: add draw code for native data here
}


// CScribbleView printing

BOOL CScribbleView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	pInfo->SetMaxPage(2);
	BOOL bRet =  DoPreparePrinting(pInfo);
	pInfo->m_nNumPreviewPages = 2;
	return bRet;
}

void CScribbleView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CScribbleView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CScribbleView diagnostics

#ifdef _DEBUG
void CScribbleView::AssertValid() const
{
	CView::AssertValid();
}

void CScribbleView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CScribbleDoc* CScribbleView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CScribbleDoc)));
	return (CScribbleDoc*)m_pDocument;
}
#endif //_DEBUG


// CScribbleView message handlers


void CScribbleView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (GetCapture() != this)
		return;
	CScribbleDoc* pDoc = GetDocument();
	CClientDC dc(this);
	OnPrepareDC(&dc);
	dc.DPtoLP(&point);
	CPen* pOldPen = dc.SelectObject(pDoc->GetCurrentPen());
	dc.MoveTo(m_ptPrev);
	dc.LineTo(point);
	dc.SelectObject(pOldPen);
	m_pStrokeCur->m_pointArray.Add(point);
	m_pStrokeCur->FinishStroke();
	pDoc->UpdateAllViews(this, 0L, m_pStrokeCur);
	ReleaseCapture();
	//CView::OnLButtonUp(nFlags, point);
}


void CScribbleView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CClientDC dc(this);
	OnPrepareDC(&dc);
	dc.DPtoLP(&point);
	m_pStrokeCur = GetDocument()->NewStroke();
	m_pStrokeCur->m_pointArray.Add(point);
	SetCapture();
	m_ptPrev = point;

	//CView::OnLButtonDown(nFlags, point);
}


void CScribbleView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (GetCapture() != this)
		return;
	CClientDC dc(this);
	OnPrepareDC(&dc);
	dc.DPtoLP(&point);
	m_pStrokeCur->m_pointArray.Add(point);
	CPen* pOldPen = dc.SelectObject(GetDocument()->GetCurrentPen());
	dc.MoveTo(m_ptPrev);
	dc.LineTo(point);
	dc.SelectObject(pOldPen);
	m_ptPrev = point;
	//CView::OnMouseMove(nFlags, point);
}


void CScribbleView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* pHint)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pHint != NULL)
	{
		if (pHint->IsKindOf(RUNTIME_CLASS(CStroke)))
		{
			CStroke* pStroke = (CStroke*)pHint;
			CClientDC dc(this);
			OnPrepareDC(&dc);
			CRect rectInvalid = pStroke->GetBoundingRect();
			dc.LPtoDP(&rectInvalid);
			InvalidateRect(&rectInvalid);
			return;
		}
	}
	Invalidate();
}


void CScribbleView::OnInitialUpdate()
{
	SetScrollSizes(MM_TEXT, GetDocument()->GetDocSize());
	CScrollView::OnInitialUpdate();

	// TODO: Add your specialized code here and/or call the base class
}


void CScribbleView::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pInfo->m_nCurPage == 1)
	{
		PrintTitlePage(pDC, pInfo);
	}
	else
	{
		CString strHeader = GetDocument()->GetTitle();
		PrintPageHeader(pDC, pInfo, strHeader);
		pDC->SetWindowOrg(pInfo->m_rectDraw.left, -pInfo->m_rectDraw.top);
		OnDraw(pDC);
	}

	//CScrollView::OnPrint(pDC, pInfo);
}


void CScribbleView::PrintTitlePage(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: Add your implementation code here.
	LOGFONT logFont;
	memset(&logFont, 0, sizeof(LOGFONT));
	logFont.lfHeight = 75;
	CFont font;
	CFont* pOldFont = NULL;
	if (font.CreateFontIndirectW(&logFont))
		pOldFont = pDC->SelectObject(&font);
	CString strPageTitle = GetDocument()->GetTitle();
	pDC->SetTextAlign(TA_CENTER);
	pDC->TextOutW(pInfo->m_rectDraw.right / 2, -100, strPageTitle);
	if (pOldFont != NULL)
		pDC->SelectObject(pOldFont);
}


void CScribbleView::PrintPageHeader(CDC* pDC, CPrintInfo* pInfo, CString& strHeader)
{
	// TODO: Add your implementation code here.
	pDC->SetTextAlign(TA_LEFT);
	pDC->TextOutW(0, -25, strHeader);
	TEXTMETRIC textMetric;
	pDC->GetTextMetrics(&textMetric);
	int y = -35 - textMetric.tmHeight;
	pDC->MoveTo(0, y);
	pDC->LineTo(pInfo->m_rectDraw.right, y);
	y -= 25;
	pInfo->m_rectDraw.top += y;
}
