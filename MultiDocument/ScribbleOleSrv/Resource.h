//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ScribbleOleSrv.rc
//
#define IDOK2                           3
#define IDC_DEFAULT_PEN_WIDTHS          3
#define IDR_ScribbleOleSrvTYPE_SRVR_IP  4
#define IDR_ScribbleOleSrvTYPE_SRVR_EMB 6
#define IDR_ScribbleOleSrvTYPE_CNTR_IP  7
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDP_FAILED_TO_CREATE            102
#define IDR_MAINFRAME                   128
#define IDR_ScribbleOleSrvTYPE          130
#define ID_WINDOW_MANAGER               131
#define IDD_PEN_WIDTHS                  310
#define IDC_THIN_PEN_WIDTHS             1000
#define IDC_THICK_PEN_WIDTHS            1001
#define ID_CANCEL_EDIT_CNTR             32768
#define ID_CANCEL_EDIT_SRVR             32769
#define ID_EDIT_CLEARALL                32771
#define ID_EDIT_CLEARALL32772           32772
#define ID_PEN_THICKLINE                32773
#define ID_PEN_THICK_OR_THINK           32774
#define ID_PEN_PENWIDTH                 32775
#define ID_PEN_PEN_WIDTHS               32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        311
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           311
#endif
#endif
