
// ContainerView.cpp : implementation of the CContainerView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "Container.h"
#endif

#include "ContainerDoc.h"
#include "ContainerItem.h"
#include "resource.h"
#include "ContainerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CContainerView

IMPLEMENT_DYNCREATE(CContainerView, CView)

BEGIN_MESSAGE_MAP(CContainerView, CView)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_COMMAND(ID_OLE_INSERT_NEW, &CContainerView::OnInsertObject)
	ON_COMMAND(ID_CANCEL_EDIT_CNTR, &CContainerView::OnCancelEditCntr)
	ON_COMMAND(ID_FILE_PRINT, &CContainerView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_SETCURSOR()
	ON_COMMAND(ID_EDIT_CLEAR, &CContainerView::OnEditClear)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CLEAR, &CContainerView::OnUpdateEditClear)
END_MESSAGE_MAP()

// CContainerView construction/destruction

CContainerView::CContainerView() noexcept
{
	m_pSelection = nullptr;
	// TODO: add construction code here

}

CContainerView::~CContainerView()
{
}

BOOL CContainerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CContainerView drawing

void CContainerView::OnDraw(CDC* pDC)
{
	if (!pDC)
		return;

	CContainerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	POSITION pos = pDoc->GetStartPosition();
	while (pos)
	{
		CContainerItem* pItem = (CContainerItem*)pDoc->GetNextItem(pos);
		pItem->Draw(pDC, pItem->m_rect);
		CRectTracker tracker;
		SetupTracker(pItem, &tracker);
		tracker.Draw(pDC);
	}
	// TODO: add draw code for native data here
	// TODO: also draw all OLE items in the document

	// Draw the selection at an arbitrary position.  This code should be
	//  removed once your real drawing code is implemented.  This position
	//  corresponds exactly to the rectangle returned by CContainerItem,
	//  to give the effect of in-place editing.

	// TODO: remove this code when final draw code is complete.
	/*if (m_pSelection != nullptr)
	{
		CSize size;
		CRect rect(10, 10, 210, 210);

		if (m_pSelection->GetExtent(&size, m_pSelection->m_nDrawAspect))
		{
			pDC->HIMETRICtoLP(&size);
			rect.right = size.cx + 10;
			rect.bottom = size.cy + 10;
		}
		m_pSelection->Draw(pDC, rect);
	}*/
}

void CContainerView::OnInitialUpdate()
{
	CView::OnInitialUpdate();


	// TODO: remove this code when final selection model code is written
	m_pSelection = nullptr;    // initialize selection

}


// CContainerView printing

BOOL CContainerView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CContainerView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CContainerView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CContainerView::OnDestroy()
{
	// Deactivate the item on destruction; this is important
	// when a splitter view is being used
   COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
   if (pActiveItem != nullptr && pActiveItem->GetActiveView() == this)
   {
      pActiveItem->Deactivate();
      ASSERT(GetDocument()->GetInPlaceActiveItem(this) == nullptr);
   }
   CView::OnDestroy();
}



// OLE Client support and commands

BOOL CContainerView::IsSelected(const CObject* pDocItem) const
{
	// The implementation below is adequate if your selection consists of
	//  only CContainerItem objects.  To handle different selection
	//  mechanisms, the implementation here should be replaced

	// TODO: implement this function that tests for a selected OLE client item

	return pDocItem == m_pSelection;
}

void CContainerView::OnInsertObject()
{
	// Invoke the standard Insert Object dialog box to obtain information
	//  for new CContainerItem object
	COleInsertDialog dlg;
	if (dlg.DoModal() != IDOK)
		return;

	BeginWaitCursor();

	CContainerItem* pItem = nullptr;
	TRY
	{
		// Create new item connected to this document
		CContainerDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);
		pItem = new CContainerItem(pDoc);
		ASSERT_VALID(pItem);

		// Initialize the item from the dialog data
		if (!dlg.CreateItem(pItem))
			AfxThrowMemoryException();  // any exception will do
		ASSERT_VALID(pItem);

        if (dlg.GetSelectionType() == COleInsertDialog::createNewItem)
			pItem->DoVerb(OLEIVERB_SHOW, this);

		ASSERT_VALID(pItem);
		// As an arbitrary user interface design, this sets the selection
		//  to the last item inserted

		// TODO: reimplement selection as appropriate for your application
		m_pSelection = pItem;   // set selection to last inserted item
		pDoc->UpdateAllViews(nullptr);
	}
	CATCH(CException, e)
	{
		if (pItem != nullptr)
		{
			ASSERT_VALID(pItem);
			pItem->Delete();
		}
		AfxMessageBox(IDP_FAILED_TO_CREATE);
	}
	END_CATCH

	EndWaitCursor();
}

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the container (not the server) causes the deactivation
void CContainerView::OnCancelEditCntr()
{
	// Close any in-place active item on this view.
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != nullptr)
	{
		pActiveItem->Close();
	}
	ASSERT(GetDocument()->GetInPlaceActiveItem(this) == nullptr);
}

// Special handling of OnSetFocus and OnSize are required for a container
//  when an object is being edited in-place
void CContainerView::OnSetFocus(CWnd* pOldWnd)
{
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != nullptr &&
		pActiveItem->GetItemState() == COleClientItem::activeUIState)
	{
		// need to set focus to this item if it is in the same view
		CWnd* pWnd = pActiveItem->GetInPlaceWindow();
		if (pWnd != nullptr)
		{
			pWnd->SetFocus();   // don't call the base class
			return;
		}
	}

	CView::OnSetFocus(pOldWnd);
}

void CContainerView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != nullptr)
		pActiveItem->SetItemRects();
}

void CContainerView::OnFilePrint()
{
	//By default, we ask the Active document to print itself
	//using IOleCommandTarget. If you don't want this behavior
	//remove the call to COleDocObjectItem::DoDefaultPrinting.
	//If the call fails for some reason, we will try printing
	//the docobject using the IPrint interface.
	CPrintInfo printInfo;
	ASSERT(printInfo.m_pPD != nullptr);
	if (S_OK == COleDocObjectItem::DoDefaultPrinting(this, &printInfo))
		return;

	CView::OnFilePrint();

}



// CContainerView diagnostics

#ifdef _DEBUG
void CContainerView::AssertValid() const
{
	CView::AssertValid();
}

void CContainerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CContainerDoc* CContainerView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CContainerDoc)));
	return (CContainerDoc*)m_pDocument;
}
#endif //_DEBUG


// CContainerView message handlers


CContainerItem* CContainerView::HitTestItems(CPoint point)
{
	// TODO: Add your implementation code here.
	CContainerDoc* pDoc = GetDocument();
	CContainerItem* pItemHit = NULL;
	POSITION pos = pDoc->GetStartPosition();
	while (pos != NULL)
	{
		CContainerItem* pItem = (CContainerItem*)pDoc->GetNextItem(pos);
		if (pItem->m_rect.PtInRect(point))
			pItemHit = pItem;
	}
	return pItemHit;
}


void CContainerView::SetSelection(CContainerItem* pItem)
{
	// TODO: Add your implementation code here.
	if (pItem == NULL || m_pSelection != pItem)
	{
		COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
		if (pActiveItem != NULL && pActiveItem != pItem)
		{
			pActiveItem->Close();
		}
		Invalidate();
		m_pSelection = pItem;
	}
}


void CContainerView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CContainerItem* pItemHit = HitTestItems(point);
	SetSelection(pItemHit);
	if (pItemHit != NULL)
	{
		CRectTracker tracker;
		SetupTracker(pItemHit, &tracker);
		UpdateWindow();
		if (tracker.Track(this, point))
		{
			Invalidate();
			pItemHit->m_rect = tracker.m_rect;
			GetDocument()->SetModifiedFlag();
		}
	}
	//CView::OnLButtonDown(nFlags, point);
}


void CContainerView::SetupTracker(CContainerItem* pItem, CRectTracker* pTracker)
{
	// TODO: Add your implementation code here.
	pTracker->m_rect = pItem->m_rect;
	if (pItem == m_pSelection)
		pTracker->m_nStyle |= CRectTracker::resizeInside;
	else
		pTracker->m_nStyle |= CRectTracker::solidLine;
	if (pItem->GetItemState() == COleClientItem::openState || pItem->GetItemState() == COleClientItem::activeUIState)
		pTracker->m_nStyle |= CRectTracker::hatchInside;
}


void CContainerView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	OnLButtonDown(nFlags, point);
	if (m_pSelection != NULL)
	{
		m_pSelection->DoVerb(GetKeyState(VK_CONTROL) < 0 ? OLEIVERB_OPEN : OLEIVERB_PRIMARY, this);
	}
	//CView::OnLButtonDblClk(nFlags, point);
}


BOOL CContainerView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	// TODO: Add your message handler code here and/or call default
	if (pWnd == this && m_pSelection != NULL)
	{
		CRectTracker tracker;
		SetupTracker(m_pSelection, &tracker);
		if (tracker.SetCursor(this, nHitTest))
			return TRUE;
	}
	return CView::OnSetCursor(pWnd, nHitTest, message);
}


void CContainerView::OnEditClear()
{
	// TODO: Add your command handler code here
	if (m_pSelection != NULL)
	{
		m_pSelection->Delete();
		m_pSelection = NULL;
		GetDocument()->UpdateAllViews(NULL);
	}
}


void CContainerView::OnUpdateEditClear(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_pSelection != NULL);
}
