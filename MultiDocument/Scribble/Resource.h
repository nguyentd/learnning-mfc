//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Scribble.rc
//
#define IDC_DEFAULT_PEN_WIDTHS          3
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_ScribbleTYPE                130
#define ID_WINDOW_MANAGER               131
#define IDD_PEN_WIDTHS                  310
#define IDC_THIN_PEN_WIDTHS             1000
#define IDC_THICK_PEN_WIDTHS            1001
#define IDC_STATIC_HELP                 1001
#define ID_EDIT_CLEARALL                32771
#define ID_PEN_THICKLINE                32772
#define ID_PEN_THICK_OR_THIN            32773
#define ID_PEN_PENWIDTH                 32774
#define ID_PEN_WIDTHS                   32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        312
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           311
#endif
#endif
