//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Container.rc
//
#define IDR_ContrTYPE_CNTR_IP           7
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDP_FAILED_TO_CREATE            102
#define IDR_MAINFRAME                   128
#define IDR_ContrTYPE                   130
#define ID_WINDOW_MANAGER               131
#define ID_CANCEL_EDIT_CNTR             32768
#define ID_EDIT_DELETE                  32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        310
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
