
// SrvrItem.cpp : implementation of the CScribbleOleSrvSrvrItem class
//

#include "stdafx.h"
#include "ScribbleOleSrv.h"

#include "ScribbleOleSrvDoc.h"
#include "SrvrItem.h"
#include "CntrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CScribbleOleSrvSrvrItem implementation

IMPLEMENT_DYNAMIC(CScribbleOleSrvSrvrItem, COleServerItem)

CScribbleOleSrvSrvrItem::CScribbleOleSrvSrvrItem(CScribbleOleSrvDoc* pContainerDoc)
	: COleServerItem(pContainerDoc, TRUE)
{
	// TODO: add one-time construction code here
	//  (eg, adding additional clipboard formats to the item's data source)
}

CScribbleOleSrvSrvrItem::~CScribbleOleSrvSrvrItem()
{
	// TODO: add cleanup code here
}

void CScribbleOleSrvSrvrItem::Serialize(CArchive& ar)
{
	// CScribbleOleSrvSrvrItem::Serialize will be called by the framework if
	//  the item is copied to the clipboard.  This can happen automatically
	//  through the OLE callback OnGetClipboardData.  A good default for
	//  the embedded item is simply to delegate to the document's Serialize
	//  function.  If you support links, then you will want to serialize
	//  just a portion of the document.

	if (!IsLinkedItem())
	{
		CScribbleOleSrvDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);
		if (pDoc)
			pDoc->Serialize(ar);
	}
}

BOOL CScribbleOleSrvSrvrItem::OnGetExtent(DVASPECT dwDrawAspect, CSize& rSize)
{
	// Most applications, like this one, only handle drawing the content
	//  aspect of the item.  If you wish to support other aspects, such
	//  as DVASPECT_THUMBNAIL (by overriding OnDrawEx), then this
	//  implementation of OnGetExtent should be modified to handle the
	//  additional aspect(s).

	if (dwDrawAspect != DVASPECT_CONTENT)
		return COleServerItem::OnGetExtent(dwDrawAspect, rSize);

	// CScribbleOleSrvSrvrItem::OnGetExtent is called to get the extent in
	//  HIMETRIC units of the entire item.  The default implementation
	//  here simply returns a hard-coded number of units.

	// TODO: replace this arbitrary size

	//rSize = CSize(3000, 3000);   // 3000 x 3000 HIMETRIC units
	CScribbleOleSrvDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	rSize = pDoc->GetDocSize();
	CClientDC dc(NULL);
	dc.SetMapMode(MM_ANISOTROPIC);
	dc.SetViewportExt(dc.GetDeviceCaps(LOGPIXELSX), dc.GetDeviceCaps(LOGPIXELSY));
	dc.SetWindowExt(100, -100);
	dc.LPtoHIMETRIC(&rSize);

	return TRUE;
}

BOOL CScribbleOleSrvSrvrItem::OnDraw(CDC* pDC, CSize& rSize)
{
	if (!pDC)
		return FALSE;

	// Remove this if you use rSize
	UNREFERENCED_PARAMETER(rSize);

	// TODO: set mapping mode and extent
	//  (The extent is usually the same as the size returned from OnGetExtent)
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowOrg(0,0);
	pDC->SetWindowExt(3000, 3000);

	// TODO: add drawing code here.  Optionally, fill in the HIMETRIC extent.
	//  All drawing takes place in the metafile device context (pDC).

	// TODO: also draw embedded CScribbleOleSrvCntrItem objects.

	// The following code draws the first item at an arbitrary position.

	// TODO: remove this code when your real drawing code is complete

	CScribbleOleSrvDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return FALSE;

	pDC->SetMapMode(MM_ANISOTROPIC);
	CSize sizeDoc = pDoc->GetDocSize();
	sizeDoc.cy = -sizeDoc.cy;
	pDC->SetWindowExt(sizeDoc);
	pDC->SetWindowOrg(0, 0);
	CTypedPtrList<CObList, CStroke*>&strokeList = pDoc->m_strokeList;
	POSITION pos = strokeList.GetHeadPosition();
	while (pos != NULL)
	{
		strokeList.GetNext(pos)->DrawStroke(pDC);
	}
	/*POSITION pos = pDoc->GetStartPosition();
	CScribbleOleSrvCntrItem* pItem = DYNAMIC_DOWNCAST(CScribbleOleSrvCntrItem, pDoc->GetNextClientItem(pos));
	if (pItem != nullptr)
		pItem->Draw(pDC, CRect(10, 10, 1010, 1010));*/
	return TRUE;
}


// CScribbleOleSrvSrvrItem diagnostics

#ifdef _DEBUG
void CScribbleOleSrvSrvrItem::AssertValid() const
{
	COleServerItem::AssertValid();
}

void CScribbleOleSrvSrvrItem::Dump(CDumpContext& dc) const
{
	COleServerItem::Dump(dc);
}
#endif

