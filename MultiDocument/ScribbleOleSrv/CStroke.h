#pragma once
#include <afx.h>
class CStroke :
	public CObject
{
public:
	CStroke(UINT nPenWidth);
	~CStroke();

protected:
	CStroke();
	DECLARE_SERIAL(CStroke)

protected:
	UINT m_nPenWidth;

public:
	CArray<CPoint, CPoint> m_poinArray;
	BOOL DrawStroke(CDC* pDC);

	virtual void Serialize(CArchive& ar);
protected:
	CRect m_rectBounding;
public:
	CRect& GetBoundingRect();
	void FinishStroke();
};

