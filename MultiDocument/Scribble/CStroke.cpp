#include "stdafx.h"
#include "CStroke.h"

IMPLEMENT_SERIAL(CStroke,CObject,3)
CStroke::CStroke()
{
}


CStroke::CStroke(UINT nPenWidth)
{
	m_nPenWidth = nPenWidth;
	m_rectBounding.SetRectEmpty();
}

CStroke::~CStroke()
{
}

BOOL CStroke::DrawStroke(CDC * pDC)
{
	CPen penStroke;
	if (!penStroke.CreatePen(PS_SOLID, m_nPenWidth, RGB(0, 0, 0)))
		return FALSE;
	CPen* pOldPen = pDC->SelectObject(&penStroke);
	pDC->MoveTo(m_pointArray[0]);
	for (int i = 1; i < m_pointArray.GetSize(); i++)
	{
		pDC->LineTo(m_pointArray[i]);
	}
	pDC->SelectObject(pOldPen);
	return TRUE;
}

void CStroke::Serialize(CArchive & ar)
{
	if (ar.IsStoring())
	{
		ar << m_rectBounding;
		ar << (WORD)m_nPenWidth;
		m_pointArray.Serialize(ar);
	}
	else
	{
		ar >> m_rectBounding;
		WORD w;
		ar >> w;
		m_nPenWidth = w;
		m_pointArray.Serialize(ar);
	}
}


void CStroke::FinishStroke()
{
	// TODO: Add your implementation code here.
	if (m_pointArray.GetSize() == 0)
	{
		m_rectBounding.SetRectEmpty();
		return;
	}CPoint pt = m_pointArray[0];
	m_rectBounding = CRect(pt.x, pt.y, pt.x, pt.y);
	for (int i = 1; i < m_pointArray.GetSize(); i++)
	{
		pt = m_pointArray[i];
		m_rectBounding.left = min(m_rectBounding.left, pt.x);
		m_rectBounding.right = min(m_rectBounding.right, pt.x);
		m_rectBounding.top = min(m_rectBounding.top, pt.y);
		m_rectBounding.bottom = min(m_rectBounding.bottom, pt.y);
	}
	m_rectBounding.InflateRect(CSize(m_nPenWidth,m_nPenWidth));
}
