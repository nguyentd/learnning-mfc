// PenWidthsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ScribbleOleSrv.h"
#include "PenWidthsDlg.h"
#include "afxdialogex.h"


// CPenWidthsDlg dialog

IMPLEMENT_DYNAMIC(CPenWidthsDlg, CDialog)

CPenWidthsDlg::CPenWidthsDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_PEN_WIDTHS, pParent)
	, m_nThinWidths(0)
	, m_nThickWidths(0)
{

}

CPenWidthsDlg::~CPenWidthsDlg()
{
}

void CPenWidthsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_THIN_PEN_WIDTHS, m_nThinWidths);
	DDV_MinMaxInt(pDX, m_nThinWidths, 1, 20);
	DDX_Text(pDX, IDC_THICK_PEN_WIDTHS, m_nThickWidths);
	DDV_MinMaxInt(pDX, m_nThickWidths, 1, 20);
}


BEGIN_MESSAGE_MAP(CPenWidthsDlg, CDialog)
	ON_BN_CLICKED(IDC_DEFAULT_PEN_WIDTHS, &CPenWidthsDlg::OnBnClickedDefaultPenWidths)
END_MESSAGE_MAP()


// CPenWidthsDlg message handlers


void CPenWidthsDlg::OnBnClickedDefaultPenWidths()
{
	// TODO: Add your control notification handler code here
	m_nThinWidths = 2;
	m_nThickWidths = 5;
	UpdateData(FALSE);
}
