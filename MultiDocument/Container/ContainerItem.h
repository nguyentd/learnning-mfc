
// ContainerItem.h : interface of the CContainerItem class
//

#pragma once

class CContainerDoc;
class CContainerView;

class CContainerItem : public COleClientItem
{
	DECLARE_SERIAL(CContainerItem)

// Constructors
public:
	CContainerItem(CContainerDoc* pContainer = nullptr);
		// Note: pContainer is allowed to be null to enable IMPLEMENT_SERIALIZE
		//  IMPLEMENT_SERIALIZE requires the class have a constructor with
		//  zero arguments.  Normally, OLE items are constructed with a
		//  non-null document pointer

// Attributes
public:
	CContainerDoc* GetDocument()
		{ return reinterpret_cast<CContainerDoc*>(COleClientItem::GetDocument()); }
	CContainerView* GetActiveView()
		{ return reinterpret_cast<CContainerView*>(COleClientItem::GetActiveView()); }

public:
	virtual void OnChange(OLE_NOTIFICATION wNotification, DWORD dwParam);
	virtual void OnActivate();
	CRect m_rect;

protected:
	virtual void OnGetItemPosition(CRect& rPosition);
	virtual void OnDeactivateUI(BOOL bUndoable);
	virtual BOOL OnChangeItemPosition(const CRect& rectPos);
	virtual BOOL OnShowControlBars(CFrameWnd* pFrameWnd, BOOL bShow);

// Implementation
public:
	~CContainerItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual void Serialize(CArchive& ar);
};

