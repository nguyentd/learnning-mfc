#pragma once
class CStroke:public CObject
{
public:
	CStroke(UINT nPenWidth);
	~CStroke();

	BOOL DrawStroke(CDC* pDC);
	virtual void Serialize(CArchive& ar);
	CArray<CPoint, CPoint> m_pointArray;

protected:
	CRect m_rectBounding;
public:
	CRect& GetBoundingRect() { return m_rectBounding; }

protected:
	CStroke();
	DECLARE_SERIAL(CStroke);

	UINT m_nPenWidth;

public:
	void FinishStroke();
};

