
// CntrItem.h : interface of the CScribbleOleSrvCntrItem class
//

#pragma once

class CScribbleOleSrvDoc;
class CScribbleOleSrvView;

class CScribbleOleSrvCntrItem : public COleClientItem
{
	DECLARE_SERIAL(CScribbleOleSrvCntrItem)

// Constructors
public:
	CScribbleOleSrvCntrItem(CScribbleOleSrvDoc* pContainer = nullptr);
		// Note: pContainer is allowed to be null to enable IMPLEMENT_SERIALIZE
		//  IMPLEMENT_SERIALIZE requires the class have a constructor with
		//  zero arguments.  Normally, OLE items are constructed with a
		//  non-null document pointer

// Attributes
public:
	CScribbleOleSrvDoc* GetDocument()
		{ return reinterpret_cast<CScribbleOleSrvDoc*>(COleClientItem::GetDocument()); }
	CScribbleOleSrvView* GetActiveView()
		{ return reinterpret_cast<CScribbleOleSrvView*>(COleClientItem::GetActiveView()); }

public:
	virtual void OnChange(OLE_NOTIFICATION wNotification, DWORD dwParam);
	virtual void OnActivate();

protected:
	virtual void OnGetItemPosition(CRect& rPosition);
	virtual void OnDeactivateUI(BOOL bUndoable);
	virtual BOOL OnChangeItemPosition(const CRect& rectPos);
	virtual BOOL OnShowControlBars(CFrameWnd* pFrameWnd, BOOL bShow);
	virtual BOOL CanActivate();

// Implementation
public:
	~CScribbleOleSrvCntrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual void Serialize(CArchive& ar);
};

