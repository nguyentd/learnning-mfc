
// ScribbleOleSrvDoc.h : interface of the CScribbleOleSrvDoc class
//


#pragma once

#include "CStroke.h"

class CScribbleOleSrvSrvrItem;

class CScribbleOleSrvDoc : public COleServerDoc
{
protected: // create from serialization only
	CScribbleOleSrvDoc() noexcept;
	DECLARE_DYNCREATE(CScribbleOleSrvDoc)

// Attributes
public:
	CScribbleOleSrvSrvrItem* GetEmbeddedItem()
		{ return reinterpret_cast<CScribbleOleSrvSrvrItem*>(COleServerDoc::GetEmbeddedItem()); }

// Operations
public:

// Overrides
protected:
	virtual COleServerItem* OnGetEmbeddedItem();
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CScribbleOleSrvDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
	UINT m_nPenWidth;
	CPen m_penCur;

public:
	CTypedPtrList<CObList, CStroke*> m_strokeList;
	CPen* GetCurrentPen() { return &m_penCur; }
	CStroke* NewStroke();
protected:
	void InitDocument();
public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual void DeleteContents();
	afx_msg void OnEditCopy();
	afx_msg void OnEditClearall();
	afx_msg void OnPenThickOrThink();
	void ReplacePen();
protected:
	BOOL m_bThickPen;
	UINT m_nThinWidth;
	UINT m_nThickWidth;
public:
	afx_msg void OnUpdateEditClearall(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePenThickOrThink(CCmdUI *pCmdUI);
	afx_msg void OnPenPenWidths();
protected:
	CSize m_sizeDoc;
public:
	CSize GetDocSize();
};
