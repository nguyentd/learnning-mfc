
// SrvrItem.h : interface of the CScribbleOleSrvSrvrItem class
//

#pragma once

class CScribbleOleSrvSrvrItem : public COleServerItem
{
	DECLARE_DYNAMIC(CScribbleOleSrvSrvrItem)

// Constructors
public:
	CScribbleOleSrvSrvrItem(CScribbleOleSrvDoc* pContainerDoc);

// Attributes
	CScribbleOleSrvDoc* GetDocument() const
		{ return reinterpret_cast<CScribbleOleSrvDoc*>(COleServerItem::GetDocument()); }

// Overrides
	public:
	virtual BOOL OnDraw(CDC* pDC, CSize& rSize);
	virtual BOOL OnGetExtent(DVASPECT dwDrawAspect, CSize& rSize);

// Implementation
public:
	~CScribbleOleSrvSrvrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
};

